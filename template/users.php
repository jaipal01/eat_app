<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>MotorBabu | Users</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <?php include("header.php"); ?>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php include("sidebar.php"); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Users
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">User List</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Users </div>
                                    <div class="actions">
                                    	<div class="btn-group">
                                            <a class="btn btn-circle btn-default btn-sm" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-user"></i> User
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="icon-user"></i> New User </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="icon-present"></i> New Event
                                                        <span class="badge badge-success">4</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="icon-basket"></i> New order </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="icon-flag"></i> Pending Orders
                                                        <span class="badge badge-danger">4</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="icon-users"></i> Pending Users
                                                        <span class="badge badge-warning">12</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <a href="javascript:;" class="btn btn-default btn-sm btn-circle">
                                            <i class="fa fa-plus"></i> Add </a>
                                        <a href="javascript:;" class="btn btn-default btn-sm btn-circle">
                                            <i class="fa fa-print"></i> Print </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th> S No. </th>
                                                <th> Name </th>
                                                <th> Mobile </th>
                                                <th> City </th>
                                                <th> No Of Booking </th>
                                                <th> No of Vehicle </th>
                                                <th> Customer Rating </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th> S No </th>
                                                <th> Name </th>
                                                <th> Mobile </th>
                                                <th> City </th>
                                                <th> No Of Booking </th>
                                                <th> No of Vehicle </th>
                                                <th> Customer Rating </th>
                                                <th> Action </th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> Bhupendra Singh Rajput </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                    <a href="user_profile.php" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 2 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 3 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 4 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 5 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 6 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 7 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 8 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 9 </td>
                                                <td> Bhupendra </td>
                                                <td> 9893940391 </td>
                                                <td> Indore </td>
                                                <td> 1 </td>
                                                <td> 2 </td>
                                                <td> 4 </td>
                                                <td>
                                                	<a href="javascript:;" class="btn btn-icon-only yellow"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <?php include("sidebar_right.php"); ?>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include("footer.php"); ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>