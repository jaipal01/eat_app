 <div class="page-sidebar-wrapper">
                
                <div class="page-sidebar navbar-collapse collapse">
                    
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                       <?php $roll = $this->session->userdata('roll');?>
                       <?php if($roll == 'admin'){?>
                        <li class="nav-item">
                                    
                                    <a href="<?php echo base_url('user/show_user')?>" class="nav-link ">
                                    <i class="fa fa-user"></i>    
                                    <span class="title">Show User</span>
                                    </a>
                        </li>
                         <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                 <a href="<?php echo base_url('user/add_country')?>"  class="nav-link nav-toggle">
                                 <i class="fa fa-globe"></i>
                                  <span class="title">Country</span>
                                  <span class="arrow"></span>
                                  </a>
                                  <ul class="sub-menu">
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('user/add_country')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-globe"></i>
                                         <span class="title">Add country</span>
                                         </a>
                                     </li>
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('user/show_country')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-globe"></i>
                                         <span class="title">Show Country</span>
                                         </a>
                                     </li>
                                 </ul>

                            </li>
                               <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                 <a href="<?php echo base_url('user/add_city')?>"  class="nav-link nav-toggle">
                                 <i class="fa fa-globe"></i>
                                  <span class="title">City</span>
                                  <span class="arrow"></span>
                                  </a>
                                  <ul class="sub-menu">
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('user/add_city')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-globe"></i>
                                         <span class="title">Add City</span>
                                         </a>
                                     </li>
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('user/show_city')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-globe"></i>
                                         <span class="title">Show city</span>
                                         </a>
                                     </li>
                                 </ul>

                                </li>
                               <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                  <a href="<?php echo base_url('user/update_version')?>"  class="nav-link nav-toggle">
                                  <i class="fa fa-code-fork"></i>
                                  <span class="title">Version</span>
                                  <span class="arrow"></span>
                                  </a>
                                   <ul class="sub-menu">
                                       <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                           <a href="<?php echo base_url('user/add_version')?>"  class="nav-link nav-toggle">
                                           <i class="fa fa-code-fork"></i>
                                           <span class="title">Upadte Version</span>
                                           </a>
                                       </li>
                                   </ul>
                               </li>
                            <?php }else{?>
                              <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                 <a href="<?php echo base_url('restaurant')?>"  class="nav-link nav-toggle">
                                 <i class="fa fa-cutlery"></i>
                                  <span class="title">Restuarant</span>
                                  <span class="arrow"></span>
                                  </a>
                                  <ul class="sub-menu">
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('restaurant')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-cutlery"></i>
                                         <span class="title">Add Restuarant</span>
                                         </a>
                                     </li>
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';}?> ">
                                         <a href="<?php echo base_url('restaurant/show_restaurant')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-cutlery"></i>
                                         <span class="title">Show Restuarant</span>
                                         </a>
                                     </li>
                                     <li class="nav-item">
                                        <a href="<?php echo base_url('restaurant/gallery')?>" class="nav-link ">
                                        <i class="fa fa-file-image-o"></i>    
                                        <span class="title">Gallery</span>
                                        </a>
                                    </li>
                                 </ul>

                            </li>
                            
                            <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                 <a href="<?php echo base_url('restaurant')?>"  class="nav-link nav-toggle">
                                 <i class="fa fa-bars"></i>
                                  <span class="title">Menu</span>
                                  <span class="arrow"></span>
                                  </a>
                                  <ul class="sub-menu">
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('restaurant/mainmenu_list')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-bars"></i>
                                         <span class="title">Menu</span>
                                         </a>
                                     </li>
                                      <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('restaurant/menu_list')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-bars"></i>
                                         <span class="title">Sub Menu</span>
                                         </a>
                                     </li>
                                 </ul>
                            </li>
                             <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                 <a href="<?php echo base_url('restaurant')?>"  class="nav-link nav-toggle">
                                 <i class="fa fa-money"></i>
                                  <span class="title">Offer</span>
                                  <span class="arrow"></span>
                                  </a>
                                  <ul class="sub-menu">
                                     <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('restaurant/add_offers')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-money"></i>
                                         <span class="title">Add Offer</span>
                                         </a>
                                     </li>
                                      <li class="nav-item <?php //if($this->uri->segment(2)=='doctor'){ echo 'active';} ?> ">
                                         <a href="<?php echo base_url('restaurant/show_offers')?>"  class="nav-link nav-toggle">
                                         <i class="fa fa-money"></i>
                                         <span class="title">Show Offer</span>
                                         </a>
                                     </li>
                                 </ul>
                            </li>
                            <?php }?>                       
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>