<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
         <style>
        .error{
            color:#ff3355;
        }
        </style>
        <meta charset="utf-8" />
        <title>Add|Sub Menu</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
       
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
      
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>template/assets/global/css/bootstrap-clockpicker.css" rel="stylesheet">
        <link href="<?php echo base_url();?>template/assets/global/css/jquery-clockpicker.css" rel="stylesheet">

        <style type="text/css">
            .boxInputRepeat{ display: inline-block; width: 100%; position: relative; margin: 12px 0px 0px;}
            .boxInputRepeat .remove_button{ margin: 0;float: left;text-align: left;}
        </style>

        <!--==partley css==-->
        <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet">  
        <link href="<?php echo base_url();?>template/assets/global/css/bootstrap-chosen.css" rel="stylesheet"> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
       
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-server"></i>Add Submenu</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('restaurant/submenu');?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate=''>
                   
                <div class="form-body">

                     <div class="form-group">
                        <label class="control-label col-md-3">Restaurant Name<span class="required"> * </span></label>
                        <div class="col-md-4">
                            <select class="form-control" name="restaurant_id" onchange="getmenu(this.value)" required="">
                                    <option value="">Select Restaurant</option>
                                    <?php if(!empty($restaurant)){
                                             foreach($restaurant as $res)
                                             {?>
                                             <option value="<?php echo $res->restaurant_id;?>"><?php echo $res->restaurant_name;?></option>
                                            <?php } 
                                        }?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Menu<span class="required"> * </span></label>
                        <div class="col-md-4">
                             <select name="menu_id" id="menu"  class="form-control" required>
                            <option value="">Select Menu</option>
                            </select>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Submenu<span class="required"> * </span></label>
                        <div class="col-md-4">
                            <input type="text" placeholder="Add Submenu" name="submenu" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Title<span class="required"> * </span></label>
                        <div class="col-md-4">
                            <input type="text" placeholder="Title" name="title" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Image<span class="required"> * </span></label>
                        <div class="col-md-4">
                            <input type="file"  name="image[]" class="form-control" multiple=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required"> * </span></label>
                        <div class="col-md-4">
                           <textarea name="description" rows="4" cols="35"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Variant Title<span class="required"> * </span></label>
                        <div class="col-md-4">
                            <input type="text" placeholder="Title" name="variant_title" class="form-control" required/>
                        </div>
                    </div>
                    <div class="field_wrapper">
                    <div class="form-group">
                        <label class="control-label col-md-3">Option <small>Attribute</small><span class="required"> * </span></label>
                        <div class="col-md-2">
                            <input type="text" placeholder="Option" name="option[]" class="form-control" required/>
                        </div>
                        <div class="col-md-2">
                            <input type="text" placeholder="Price" name="price[]" class="form-control" required/>
                        </div><!-- <button type="button"  name='addmore' class="add_button"><i class="fa fa-plus"></i></button>-->
                       <!--  <a href="#" class="add_button control-label col-md-4"><i class="fa fa-add"></i></a> -->
                        <span class="input-group-btn"><button type="button" class="add_button btn btn-success btn-number" data-type="plus" data-field="quant[2]"><span class="glyphicon glyphicon-plus"></span></button> </span>
                    </div>
                </div>
                  <div class="form-group">
                        <label class="control-label col-md-3">Select Related Product <span class="required"> * </span></label>
                        <div class="col-md-4">
                        <?php if(!empty($submenu)) {?>
                        <div>   
                        <select data-placeholder="Choose Your Product" id="" multiple class="chosen-select" name="product[]" tabindex="8">
                              <?php foreach($submenu as $value) { ?>
                                                        
                                <option value='<?php echo $value->submenu;?>'><?php echo $value->submenu;?></option>

                                <?php }}else{ echo '<span>'.'Please Add First Submenu'.'</span>'; }?>
                        </select>
                        </div>
                        </div>
                     </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" value="Submit"  name="submit">
                            <a href="<?php echo base_url('restaurant/submenu')?>"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
      
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>

        <script src="<?php echo base_url();?>template/assets/global/js/chosen.jquery.js" rel="stylesheet"> </script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/jquery-clockpicker.min.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/bootstrap-clockpicker.min.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
   
        <!-- END THEME LAYOUT SCRIPTS -->
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>        

<script type="text/javascript">
$('.clockpicker').clockpicker({donetext: 'Done'});
</script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="boxInputRepeat"><label class="control-label col-md-3">Option <small>Attribute</small><span class="required"> * </span></label><div class="col-md-2"><input type="text" placeholder="Option" name="option[]" class="form-control" required/></div><div class="col-md-2"><input type="text" placeholder="Price" name="price[]" class="form-control" required/></div><div><a href="javascript:void();" class="remove_button add_button btn btn-danger btn-number"><span class="glyphicon glyphicon-minus"></span></a></div></div>'; //New input field html 

      var x = 1; //Initial field counter is 1
        $(addButton).click(function(){ //Once add button is clicked
            if(x < maxField){ //Check maximum number of input fields
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); // Add field html
            }
        });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});

function getmenu(Restaurant_id)
 { 
   var str = "restaurant_id="+Restaurant_id;
   
   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>restaurant/get_menu/",
        data:str,
        success:function(data)
        { 
          $('#menu').empty();
          $('#menu').append(data);
        }
     });
     $('#menu option').attr('selected', false);
 }   
</script>
       
<script type="text/javascript">
  $('#form11').parsley();  
</script>



    </body>

</html>
