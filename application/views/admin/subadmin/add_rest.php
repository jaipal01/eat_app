<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
         <style>
        .error{
            color:#ff3355;
        }
        </style>
        <meta charset="utf-8" />
        <title>Add|Restaurant</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
       
        <link href="<?php echo base_url()?>template/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
      
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url()?>template/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>template/assets/global/css/bootstrap-clockpicker.css" rel="stylesheet">
        <link href="<?php echo base_url();?>template/assets/global/css/jquery-clockpicker.css" rel="stylesheet">

        <!--==partley css==-->
        <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet">  
        <link href="<?php echo base_url();?>template/assets/global/css/bootstrap-chosen.css" rel="stylesheet"> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
       
        <link rel="shortcut icon" href="favicon.ico" /> </head>
        <!-- END HEAD -->
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                   
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-cutlery"></i>Add Restaurant</div>
                                               
                                            </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
             <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('restaurant/add_restaurant');?>" onsubmit="return myFunction()" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate=''>
                   
                <div class="form-body">

                    <div class="form-group">
                        <label class="control-label col-md-3">Select Cuisine <span class="required"> * </span></label>
                        <div class="col-md-7">
                        <?php if(!empty($cuisine)) {?>
                        <div>   
                        <select data-placeholder="Choose Your Cuisine" id="" multiple class="chosen-select" name="cuisine_name[]" tabindex="8" required>
                              <?php foreach($cuisine as $value) { ?>
                                                        
                                <option value='<?php echo $value->cuisine_name;?>'><?php echo $value->cuisine_name;?></option>

                                <?php }}?>
                        </select>
                        </div>
                        </div>
                     </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Restaurant Name<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Restaurant Name" name="restaurant_name" class="form-control" value=""  required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">About Restaurant<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <textarea name="about_restaurant" class="form-control" rows="4" required></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Open time<span class="required"> * </span></label>
                        <div class="col-md-2">
                            <div class="input-group clockpicker">
                                <input type="text" name="open_time" class="form-control" id="open_time" data-autoclose="true" required>
                                 <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                 </span>
                             </div>
                        </div>
                        <label class="control-label col-md-3">Closed time <span class="required"> * </span></label>
                        <div class="col-md-2">
                             <div class="input-group clockpicker">
                                <input type="text" name="closed_time"  id="closed_time" class="form-control" data-autoclose="true" required>
                                 <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                 </span>
                             </div>
                        </div> 
                        <span id="ab" style="color:red;margin-left:266px;list-style-type: none;font-size: 0.9em;line-height: 0.9em;"></span> 

                    </div>
                      

                    <div class="form-group">
                        <label class="control-label col-md-3">Country<span class="required"> * </span></label>
                        <div class="col-md-7">
                             <select name="country" id="" onchange="getcity(this.value)" class="form-control" required>
                            <option value="">Select Country</option>
                            <?php if(!empty($country))
                                 foreach($country as $key) 
                                 {?>
                                 <option value="<?php echo $key->id;?>"><?php echo $key->name;?></option>

                               <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Select City</label>
                        <div class="col-md-7">
                            <select name="city" id="city" class="form-control" >
                            <option value="">Select City</option>
                           
                            </select>
                            <?php echo form_error('health_cate',"<div id='error'>", "</div>") ?>
                        </div>
                    </div>
                </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Address<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Address" name="address" class="form-control" value=""  required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Contact Number<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Contact Number" name="contact_number" class="form-control" value="" data-parsley-type="number" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Image<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="file" name="image" class="form-control" value="upload_image" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Payment Method <span class="required"> * </span></label>
                        <div class="col-md-7">
                       <div>
                        <select data-placeholder="Choose Your Payment Method" id="" multiple class="chosen-select" name="method[]" tabindex="8" required>
                                <option value='Cash on Delivery'>Cash on Delivery</option>
                                <option value='Net Banking'>Net Banking</option>
                        </select>
                        </div>
                        </div>
                     </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Delivery Service <span class="required"> * </span></label>
                        <div class="col-md-7">
                            <select name="delivery_service" class="form-control">
                                <option value="" selected="select">Select Services</option>
                                <option value="1" >Delivery Service</option>
                                <option value="2" >Pickup Service</option>
                                <option value="3" >Both</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Delivery Charges<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Delivery Charges" name="delivery_Charges" class="form-control" value="" data-parsley-type="number" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Delivery Time<span class="required"> * </span></label>
                        <div class="col-md-7">
                           <!--  <input type="text" placeholder="Delivery Time" name="delivery_Time" class="form-control" value="" data-parsley-type="number" /> -->
                           <div class="input-group clockpicker">
                                <input type="text" name="delivery_Time" class="form-control" data-autoclose="true" required>
                                 <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                 </span>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Special<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="specials" name="special" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Minimum order<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Minimum order value" name="minimum_order_value" class="form-control" value="" data-parsley-type="number" required/>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" value="Submit" name="submit">
                            <a href="<?php echo base_url('restaurant')?>"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
            
            <!-- END FORM-->
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
       
     <?php $this->load->view('admin/footer'); ?>
      
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>

        <script src="<?php echo base_url();?>template/assets/global/js/chosen.jquery.js" rel="stylesheet"> </script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/jquery-clockpicker.min.js"></script>
        <script src="<?php echo base_url()?>template/assets/global/js/bootstrap-clockpicker.min.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url()?>template/assets/global/scripts/app.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
       
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>template/ckeditor/ckeditor.js"></script>
   
        <!-- END THEME LAYOUT SCRIPTS -->
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>        

<script type="text/javascript">
$('.clockpicker').clockpicker({donetext: 'Done'});
</script>
       
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
 $( function() {
   $( "#datepicker" ).datepicker({
     changeYear: true,
     dateFormat : 'dd/mm/yy',
     changeMonth: true,
     // maxDate: '-1d'
      maxDate: new Date(1998,0,3),

   });
 } );
 </script>
 <script type="text/javascript">
 function getcity(country_id)
 { 
   var str = "country_id="+country_id;
  
   $.ajax({
        type:"POST",
        url:"<?php echo base_url();?>restaurant/get_city/",
        data:str,
        success:function(data)
        { 
          $('#city').empty();
          $('#city').append(data);
        }
     });
     $('#city option').attr('selected', false);
 }   

 function myFunction()
 {
    var opentime = document.getElementById("open_time").value;
    var closed_time = document.getElementById("closed_time").value;

    if(closed_time < opentime)
    {
        $('#ab').html('Closed time should not be less from open time.');
        return false;
    }    
 }          
</script>
    </body>

</html>
