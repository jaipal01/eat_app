<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if($userid = $this->session->userdata('admin_id')){
			redirect(base_url('dashboard'));
		}
		
	}
	
	public function index()
	{ 
		if(isset($_POST['submit']))
		{   
			     $username = $this->input->post ( 'username' );
				 $password = $this->input->post('password') ;
				
				$AdminData = $this->common_model->common_getRow('admin',array('email'=>$username,'password'=>md5($password)));
				if (!empty($AdminData)) 
				{
						$this->session->set_userdata ( array (
								'admin_id'   => $AdminData->admin_id,
								'username'   => $AdminData->email,
								'roll'       => $AdminData->roll,
														));
						   if($AdminData->roll == 'admin')
						   {
							redirect(base_url().'user/show_user');
						   }
						   else{
						   	redirect(base_url().'restaurant'); 
						   }
					
				}
				else
				{
				  $this->session->set_flashdata('msg' ,'Email or Password Not Matched.');	
				  redirect(base_url('login'));	
				}	
				
		}
		$this->load->view('admin/index');
		
	}
	
	
}
