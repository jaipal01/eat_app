<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
		/*define('GOOGLE_API_KEY','AAAA_ouBbIE:APA91bHGdRtlyUlKKOoE7BqIoG2ztXbcSAGnMlFXpnlLlem29e9Gr7TEc2ZGiSlJ7tbFrYZJLfoVNYBPeSE0n2T4U0oZpejYx3CisSLiifC_xAnw5KXUY0oRlb8T8_bYyioiRmH1voQM');*/
	}
	
public function register()
{ 
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$mobileno = $this->input->post('mobile_no');
		$offer_notification = $this->input->post('offer_notification');
		$country_id = $this->input->post('country_id');
		$country_name = $this->input->post('country_name');


		if(!empty($email))
	    {
		 
			     	$final_output = array();
			     	$check_row = $this->db->query("SELECT * FROM `user` WHERE `email`= '$email'");
			     	$check_row1=$check_row->row();
			     	if($check_row1)
			     	{ 
			     		if($check_row1->email_verify == 0)
			     		{
			     			$data_array = array(
						                  'fullname'=>$username,
						                  'password'=>md5($password),
						                  'mobile_no'=>$mobileno,
						                  'offer_notification'=>$offer_notification,
						                  'mobile_country_code'=>$country_id,
						                  'country'=>$country_name,
						                  'create_at'=>militime,
						                  'update_at'=>militime,
						                  'admin_status'=>1,
						                  'email_verify'=>0
						                  );
			     			$update_value=$this->common_model->updateData('user',$data_array,array('email'=>$email));

		     				if($update_value)
		     				{
		     					    $userid = $check_row1->user_id;
		     					
									$final_output['status'] = 'true';
									$final_output['message'] = 'successfully';
									$final_output['msg_type'] = 1;		
									$final_output['msg'] = 'Please check your email if you have already verification email OR click on resend button for email verification link.';	
							}
							else
							{
								$final_output['status'] = 'false';	
			     			    $final_output['message'] = 'failed';
			     			    $final_output['msg_type'] = 0;	
							    $final_output['msg'] = 'Something went wrong please try again!!';
							}
			     		}
			     		else
			     		{	
			     			if($check_row1->type == 1)
			     			{
			     				$final_output['status'] = 'false';	
			     			    $final_output['message'] = 'failed';
			     			    $final_output['msg_type'] = 1;	
							    $final_output['msg'] = 'You have already registered with facebook! Please Login via facebook.';
							}else
			     			{
				     			$final_output['status'] = 'false';	
			     			    $final_output['message'] = 'failed';
			     			    $final_output['msg_type'] = 1;	
							    $final_output['msg'] = 'Email Already Exist Please Login.';
							}	
			     		}
			     	}
			     	else
			     	{
			     		$data_array = array(
						                  'fullname'=>$username,
						                  'email'=>$email,
						                  'password'=>md5($password),
						                  'mobile_no'=>$mobileno,
						                  'offer_notification'=>$offer_notification,
						                  'mobile_country_code'=>$country_id,
						                  'country'=>$country_name,
						                  'create_at'=>militime,
						                  'update_at'=>militime,
						                  'admin_status'=>1
						                  );
							$insertId = $this->common_model->common_insert('user', $data_array);

							if($insertId)
							{  
								$milliseconds = number_format(round(microtime(true) * 1000),0,'','');
                  				$Verification_Code = substr($milliseconds, -7);



							  $verification_url = base_url()."email_template/q.php?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($email)."";
							  $messageproper ="<html><body><table>".
										                  "<tr><td>Hello ".$username.",</td><tr>".
										                  "<tr><td><br />Thank you for registering Your account is created and must be verified before you can use it. <br /><br/></td><tr>". 
										                  "<tr><td><strong><a href='".$verification_url."''>Click here</strong></a> to verify your accout.<br /><br/></td><tr>".
										                  "<tr><td><strong>Verification code:</strong> ".$Verification_Code."<br /><br/></td><tr>".
										                  "<tr><td>or copy-paste the following link in your browser: ".$verification_url."<br /><br/></td><tr>".
										                  "<tr><td><strong>Username:</strong> ".$email."</td><tr>".
										                  "<tr><td><br /><br/>
										                  Admin Team<br/>
										                  <strong>Eatapp</strong></td><tr>".   
										                  "</table></body></html>";

			                                    $subject = "Eatapp App: Verification Link";
			                                    $email_from ='no-reply@eatapp.com';
			                                    $headers  = 'MIME-Version: 1.0' . "\r\n";
			                                    $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
			                                    $headers .= 'From: '.$email_from. '\r\n';
			                                    $cc = '';
			                                   
			                            @mail($email, $subject, $messageproper,$headers);
			     						$update_value=$this->common_model->updateData('user',array('code'=>$Verification_Code),array('user_id'=>$insertId));

										$final_output['status'] = 'true';
										$final_output['message'] = 'successfully';
										$final_output['msg_type'] = 0;		
										$final_output['msg'] = 'You have Registered successfully and verification link has been sent to your email account.';	
							}
							else
							{ 
							$final_output['status'] = 'false';
							$final_output['message'] = 'failed';		
							$final_output['msg_type'] = 0;		
							$final_output['msg'] = 'Something went wrong!! Please try again.';	
							}
					}
			
	    }
	    else
	    {
	    	$final_output['status'] = 'false';
			$final_output['message'] = 'failed';
			$final_output['msg_type'] = 0;		
	     	$final_output['msg'] = 'No Request parameter.';
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output);

}

public function login() 
{
	$email = $this->input->post('email');
	$password = md5($this->input->post('password'));
	$device_token = $this->input->post('device_token');
	$device_id = $this->input->post('device_id');
	$device_type = $this->input->post('device_type');

	if(!empty($email))	
	{
	     	$final_output = array();
		    $check_email = $this->common_model->common_getRow('user',array('email'=>$email));

		if($check_email)
		{  
		        $existing_password = $check_email->password;
		     	
		     	if($existing_password == $password)
		     	{
		     		$auth_key = $this->rand_string(40);

		     		if($check_email->admin_status == 1)
		     		{
		     				if($check_email->email_verify == 1)
							{
								$uid = $check_email->user_id;
								$update_value = $this->common_model->updateData('user',array('update_at'=>militime,'token'=>$auth_key,'device_token'=>$device_token,'device_id'=>$device_id,'device_type'=>$device_type),array('user_id'=>$uid));

						        if($update_value)
							    {  
							    		if(!empty($check_email->image))
							    		{
							    			 if(strpos($check_email->image, 'http') !== false)
                           			         {
                               			        $image = $check_email->image;
			                                 }else
			                                 {
			                                   $image = base_url().'uploads/user_image/'.$check_email->image;
			                                 }
							    		}
							    		else
							    		{
							    			$image = '';
							    		}	
							    	   


										$data_array = array(
															'user_id'=>$uid,
								                  			'fullname'=>$check_email->fullname,
								                  			'email'=>$check_email->email,
								            				'mobile_no'=>$check_email->mobile_no,
								                  			'offer_notification'=>$check_email->offer_notification,
								                  			'login_type'=>$check_email->type,
								                  			'gender'=>$check_email->gender,
								                  			'country'=>$check_email->country,
								                  			'image'=>$image,
								                  			'location'=>$check_email->location,
								                  			'dob'=>$check_email->dob,
								                  			'token'=>$auth_key,
								                  			'country_id'=>$check_email->mobile_country_code
								                  			);						         					
     				   					$query = $this->db->query("UPDATE `user` SET `device_token` = '' WHERE `device_id` = '$device_id' AND `user_id` != '$uid'");
										$final_output['status'] = 'true';
										$final_output['message'] = 'successfully';
										$final_output['msg'] = 'You have logged-in successfully.';	
										$final_output['data'] = $data_array;
								   
								}
								else
								{
									$final_output['status'] = 'false';	
					     			$final_output['message'] = 'failed';	
					     			$final_output['msg_type'] = 0;
									$final_output['msg'] = 'Something went wrong please try again.';
								}
							
				     		}
				     		else
				     		{
				     				$final_output['status'] = 'false';	
					     			$final_output['message'] = 'failed';
					     			$final_output['msg_type'] = 2;	
									$final_output['msg'] = 'Please check your email if you have already verification email OR click on resend button for email verification link.';
				     		}

					}
					else
					{
						$final_output['status'] = 'false';	
		     			$final_output['message'] = 'failed';	
		     			$final_output['msg_type'] = 1;
						$final_output['msg'] = 'Your Account Temparary suspended because of security reason.';
					}

				}
				else
				{
					if($check_email->type == 1)
	     			{
	     				$final_output['status'] = 'false';	
	     			    $final_output['message'] = 'failed';
	     			    $final_output['msg_type'] = 1;	
					    $final_output['msg'] = 'You have already registered with facebook! Please Login via facebook.';
					}else
	     			{
		     			$final_output['status'] = 'false';	
	     			    $final_output['message'] = 'failed';
	     			    $final_output['msg_type'] = 0;	
					    $final_output['msg'] = 'Invalid Password.';
					}	
					
				}	

	    }
		else
		{
     		$final_output['status'] = 'false';	
     		$final_output['message'] = 'failed';
     		$final_output['msg_type'] = 0;
			$final_output['msg'] = 'Please you have to register first.';	
		}
	}
    else
    {
     	$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
		$final_output['msg_type'] = 0;
     	$final_output['msg'] = 'No Request parameter.';
    }
    header("content-type: application/json");
	echo json_encode($final_output);
}


function rand_string($length) {
$str="";
$chars = "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
$size = strlen($chars);
for($i = 0;$i < $length;$i++) {
$str .= $chars[rand(0,$size-1)];
}
return $str;
}

public function email_verify()
{
  	$id=$this->input->get('8f14e45fceea167a5a36dedd4bea2543');
  	$update_value=$this->common_model->updateData('user',array('email_verify'=>1),array('user_id'=>$id));
  		if($update_value)
  		{	
			$final_output['status'] = 'true';
			$final_output['message'] = 'successfully';
			$final_output['msg'] = 'Email verification is successful.';	
			
  		}
  		header("content-type: application/json");
		echo json_encode($final_output);
}

	public function resend_email()
	{ 
		$email=$this->input->post('email');
		if(!empty($email))	
		{
			$check_row = $this->db->query("SELECT * FROM `user` WHERE `email`= '$email'");
			$check_email = $check_row->row();
			if($check_email)
			{
				//$userid = $check_email->user_id;
			    $username = $check_email->fullname;
                $milliseconds = number_format(round(microtime(true) * 1000),0,'','');
               	$Verification_Code = substr($milliseconds, -7);
	   			$verification_url = base_url()."email_template/q.php?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($email)."";
			  	$messageproper ="<html><body><table>".
						                  "<tr><td>Hello ".$username.",</td><tr>".
						                  "<tr><td><br />Thank you for registering Your account is created and must be verified before you can use it. <br /><br/></td><tr>". 
						                  "<tr><td><strong><a href='".$verification_url."''>Click here</strong></a> to verify your accout.<br /><br/></td><tr>".
						                  "<tr><td><strong>Verification code:</strong> ".$Verification_Code."<br /><br/></td><tr>".
						                  "<tr><td>or copy-paste the following link in your browser: ".$verification_url."<br /><br/></td><tr>".
						                  "<tr><td><strong>Username:</strong> ".$email."</td><tr>".
						                  "<tr><td><br /><br/>
						                  Admin Team<br/>
						                  <strong>Eatapp</strong></td><tr>".   
						                  "</table></body></html>";

			                                    $subject = "Eatapp App: Verification Link";
			                                    $email_from ='no-reply@eatapp.com';
			                                    $headers  = 'MIME-Version: 1.0' . "\r\n";
			                                    $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
			                                    $headers .= 'From: '.$email_from. '\r\n';
			                                    $cc = '';

			                                    //echo 'jweeghu';exit;
			                             $update_value=$this->common_model->updateData('user',array('code'=>$Verification_Code),array('email'=>$email));

			                               if(@mail($email, $subject, $messageproper,$headers))
			                               { 
											$final_output['status'] = 'true';
											$final_output['message'] = 'successfully';
											$final_output['msg'] = 'Verification link has been sent successfully.';

											}
											else
											{
												$final_output['status'] = 'false';
												$final_output['message'] = 'failed';
												$final_output['msg'] = 'Something went wrong.';
											}
			}
			else
			{
				$final_output['status'] = 'false';	
	     		$final_output['message'] = 'failed';
	     		$final_output['msg_type'] = 0;
				$final_output['msg'] = 'Please you have to register first.';	
			}
		}
		else
		{
			$final_output['status'] = 'false';
			$final_output['message'] = 'failed';
			$final_output['msg_type'] = 0;
	     	$final_output['msg'] = 'No Request parameter.';
		}
		header("content-type: application/json");
		echo json_encode($final_output);
    }
	public function forgot_password()
	{
		$email=$this->input->post('email');
		if(!empty($email))
	    {
	    	$check_email = $this->common_model->common_getRow('user',array('email'=>$email));
	    	if($check_email)
			{	
				if($check_email->admin_status == 1)
				{
					if($check_email->email_verify == 1)	
					{
						if($check_email->type == 0)
						{
						 // $new_password = $this->rand_string(8);

							$verification_url = base_url()."email_template/resetpassword.php?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($email)."";
						    $messageproper ="<html><body><table>".
															//"<tr><td><br />Your New Password is: <br /><br/></td><tr>".
										                     "<tr><td><strong><a href='".$verification_url."''>Click here</strong></a> to reset your password.<br /><br/></td><tr>".
															// "<tr><td><strong>Password:</strong> ".$new_password."</td><tr>".
															"</table></body></html>";

											$subject = "Eatapp App: Forgot Password";
		                                    $email_from ='no-reply@eatapp.com';
		                                    $headers  = 'MIME-Version: 1.0' . "\r\n";
		                                    $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
		                                    $headers .= 'From: '.$email_from. '\r\n';
		                                    $cc = '';

		                                    @mail($email, $subject, $messageproper,$headers);
											//$data_array = array('password'=>md5($new_password));
			     							//$update_value=$this->common_model->updateData('user',$data_array,array('email'=>$email));

			     							$final_output['status'] = 'true';
											$final_output['message'] = 'successfully';
											$final_output['msg'] = 'Your reset password link has been sent in your Email account.';
						}
						else
						{
							$final_output['status'] = 'false';	
			     			$final_output['message'] = 'failed';	
			     			$final_output['msg_type'] = 1;
							$final_output['msg'] = 'This email id is already register with facebook account.Please login via facebook email id.';
						}						
					}
					else
					{
						$final_output['status'] = 'false';	
		     			$final_output['message'] = 'failed';	
		     			$final_output['msg_type'] = 2;
						$final_output['msg'] = 'This email is not verified,please click on resend button for email verification link.';
					}
				}
				else
				{
					$final_output['status'] = 'false';	
     			    $final_output['message'] = 'failed';
     			    $final_output['msg_type'] = 1;	
				    $final_output['msg'] = 'Your Account Temparary suspended because of security reason.';
				}
			} 
			else
			{
				$final_output['status'] = 'false';	
	     		$final_output['message'] = 'failed';
	     		$final_output['msg_type'] = 0;
				$final_output['msg'] = 'This Email is not exist.';
			}
		
	    }
	    else
	    {
	    	$final_output['status'] = 'false';
			$final_output['message'] = 'failed';
			$final_output['msg_type'] = 0;
		 	$final_output['msg'] = 'No Request parameter.';
	    }
		    header("content-type: application/json");
			echo json_encode($final_output);
	}
	
public function version_update()
{
   $version = $this->common_model->common_getRow('App_version',array('version_id'=>'1'));

   $final_output = array();
   if(!empty($version))
   {
   	 $arr =  array('version_code'=>$version->version_code,
		   	  	  'required'=>$version->update_requirment,
		   	  	  'heading'=>'Update New Version','msg'=>'We no longer support this version, Please update the application.');

   	  $final_output['status'] = 'true';
   	  $final_output['message'] = 'successfully';
   	  $final_output['data'] = $arr;
       
   }
   else
   {
   	  $final_output['status'] = 'false';
   	  $final_output['message'] = 'failed';
   	  $final_output['msg']='Something went wrong! please try again later.';
   	  unset($final_output['data']);
   }	

    header("content-type: application/json");
    echo json_encode($final_output);

}

public function login_with_facebook()
{
	$email = $this->input->post('email');
	$facebook_id = $this->input->post('facebook_id');
	$name = $this->input->post('name');
	$image = $this->input->post('image');
	$gender = $this->input->post('gender');
	$device_token = $this->input->post('device_token');
	$device_id = $this->input->post('device_id');
	$device_type = $this->input->post('device_type');

	$auth_key = $this->rand_string(40);

    if(!empty($email))
    {
            $final_output = array();
		    $check_email = $this->common_model->common_getRow('user',array('email'=>$email));

		    if(!empty($check_email))
		    {
		    	if($check_email->admin_status == 1)
		    	{

		    		$update_value = $this->common_model->updateData('user',array('update_at'=>militime,'token'=>$auth_key,'device_token'=>$device_token,'device_id'=>$device_id,'device_type'=>$device_type,'email_verify'=>1),array('email'=>$email));

			    	  if($update_value)
			    	  {
			    	  	    if(!empty($check_email->image))
							{
				    			 if(strpos($check_email->image, 'http') !== false)
	           			         {
	               			        $image = $check_email->image;
	                             }else
	                             {
	                               $image = base_url().'uploads/user_image/'.$check_email->image;
	                             }
							}
							else
							{
							   $image = '';
							}	


			    	  		$dataa_array = array(
													'user_id'=>$check_email->user_id,
						                  			'fullname'=>$check_email->fullname,
						                  			'email'=>$check_email->email,
						            				'mobile_no'=>$check_email->mobile_no,
						                  			'offer_notification'=>$check_email->offer_notification,
						                  			'login_type'=>$check_email->type,
						                  			'gender'=>$check_email->gender,
						                  			'country'=>$check_email->country,
						                  			'image'=>$image,
						                  			'location'=>$check_email->location,
						                  			'dob'=>$check_email->dob,
						                  			'country_id'=>$check_email->mobile_country_code,
						                  			'token'=>$auth_key
						                  			);

		    	  		    $final_output['status'] = 'true';
							$final_output['message'] = 'successfully';
	 						$final_output['msg'] = 'You have logged-in successfully.';	
							$final_output['data'] = $dataa_array;
			    	  }else
			    	  {
			    	  	    $final_output['status'] = 'false';
							$final_output['message'] = 'failed';
					     	$final_output['msg'] = 'Something went wrong! please try again.';
					     	unset($final_output['data']);
			    	  }	

		    	}else
		    	{
		    		$final_output['status'] = 'false';	
     			    $final_output['message'] = 'failed';
				    $final_output['msg'] = 'Your Account Temparary suspended because of security reason.';
		    	}	
		    }else
		    {
		    	$data_array = array(
					                  'fullname'=>$name,
					                  'email'=>$email,
					                  'facebook_id'=>$facebook_id,
					                  'gender'=>$gender,
					                  'image'=>$image,
					                  'offer_notification'=>1,
					                  'create_at'=>militime,
					                  'update_at'=>militime,
					                  'admin_status'=>1,
					                  'email_verify'=>1,
					                  'device_id'=>$device_id,
					                  'device_token'=>$device_token,
					                  'device_type'=>$device_type,
					                  'token'=>$auth_key,
					                  'type'=>1
						            );

				  $insertId = $this->common_model->common_insert('user', $data_array);

				  if($insertId)
				  {
							$dataa_array = array(
												'user_id'=>$insertId,
					                  			'fullname'=>$name,
					                  			'email'=>$email,
					            				'mobile_no'=>'',
					            				'country_id'=>'',
					                  			'offer_notification'=>1,
					                  			'login_type'=>1,
					                  			'gender'=>$gender,
					                  			'country'=>'',
					                  			'image'=>$image,
					                  			'location'=>'',
					                  			'dob'=>'',
					                  			'token'=>$auth_key
					                  			);

							$final_output['status'] = 'true';
							$final_output['message'] = 'successfully';
     						$final_output['msg'] = 'You have logged-in successfully.';	
							$final_output['data'] = $dataa_array;
				  }else
				  {
				  		$final_output['status'] = 'false';
						$final_output['message'] = 'failed';
				     	$final_output['msg'] = 'Something went wrong! please try again.';
				     	unset($final_output['data']);
				  }	

		    }

    }else
    {
     	$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
     	$final_output['msg'] = 'No Request parameter.';
    }
    header("content-type: application/json");
	echo json_encode($final_output);

}
public function change_password()
{
	$headers = apache_request_headers();
	$old_pass = $this->input->post('oldpass');
	$new_pass = $this->input->post('newpass');

	if($headers['secret_key'] !='')
   {
   	  $check_key = $this->checktoken($headers['secret_key']);
   	   
   	   if($check_key['status'] == 'true' && $check_key['data']->admin_status == 1)
	   {
	   		if($check_key['data']->type == 0)
	    	{		
		   		$user_id = $check_key['data']->user_id;
		   	    $final_output = array();

		   	    if($check_key['data']->password == md5($old_pass))
		   	    {
					$this->common_model->updateData('user',array('password'=>md5($new_pass)),array('user_id'=>$user_id));
		   	   		
		   	   		$final_output['status'] = 'true';
					$final_output['message'] = 'successfully';
					$final_output['msg'] = 'Password has been changed successfully.';

		   	    }
		   	    else
		   	    {
		   	    	$final_output['status'] = 'false';	
				    $final_output['message'] = 'failed';
			    	$final_output['msg'] = 'Password does not match.';
		   	    }
	   		}
	   		else
	   		{
			   		$final_output['status'] = 'false';	
				    $final_output['message'] = 'failed';
			    	$final_output['msg'] = 'You have already logged-in via facebook,You dont have access for change password functionality.';
	   		}
	   }
	   else
	   {
	   	  $final_output['status'] = 'false';
          $final_output['message'] = 'failed';
	      $final_output['msg'] = 'Invalid Token';
	   }
   }
   else
   {
   	 $final_output['status'] = 'false';
	 $final_output['message'] = 'failed';
 	 $final_output['msg'] = 'Unauthorised Access';
   }
   header("content-type: application/json");
   echo json_encode($final_output);
}

public function profile_update()
{
   $headers = apache_request_headers();

   $username =  $this->input->post('username');
   $country = $this->input->post('country');
   $location = $this->input->post('location');
   $mobileno = $this->input->post('mobile_no');
   $dob = $this->input->post('dob');
   $gender = $this->input->post('gender');
   $offer_notification = $this->input->post('offer_notification');
   $mobile_country_code = $this->input->post('country_id');

   if($headers['secret_key'] !='')
   {
   	   	$check_key = $this->checktoken($headers['secret_key']);

   	   	if($check_key['status'] == 'true' && $check_key['data']->admin_status == 1)
	    {
		   	   $user_id = $check_key['data']->user_id;
		   	   $final_output = array();

		   	   	if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
			    {
			   		$date = date("ymdhis");
					$config['upload_path'] = 'uploads/user_image/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';

					$subFileName = explode('.',$_FILES['image']['name']);
					$ExtFileName = end($subFileName);
					$config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if($this->upload->do_upload('image'))
					{ 
	                     $upload_data = $this->upload->data();
						 $image = $upload_data['file_name'];
						 $image1 = base_url().'uploads/user_image/'. $image;
					}
					
			   }
			   else
			   {
			          	if(!empty($check_key['data']->image))
						{
				    			 if(strpos($check_key['data']->image, 'http') !== false)
	           			         {
	               			        $image = $check_key['data']->image;
	               			        $image1 = $check_key['data']->image;
	                             }else
	                             {
	                               $image1 = base_url().'uploads/user_image/'.$check_key['data']->image;
	                               $image = $check_key['data']->image;
	                             }
						}
						else
						{
					      $image1 = '';
					      $image = '';
					    }	
			   } 
	   		    $update_arr = array('fullname'=>$username,'image'=>$image,'country'=>$country,'location'=>$location,'mobile_no'=>$mobileno,'mobile_country_code'=>$mobile_country_code,'dob'=>$dob,'gender'=>$gender,'offer_notification'=>$offer_notification,'update_at'=>militime);
			 	$update = $this->common_model->updateData("user",$update_arr,array('user_id' => $user_id));

			 	if($update)
		 		{
		 		 	$data_array = array(
										'fullname'=>$username,
			                  			'image'=>$image1,
			                  			'country'=>$country,
			                  			'location'=>$location,
			            				'mobile_no'=>$mobileno,
			            				'dob'=>$dob,
			            				'gender'=>$gender,
			            				'offer_notification'=>$offer_notification,
			            				'mobile_country_code'=>$mobile_country_code
			                  			);						         					

		 		 	$final_output['status'] = "true";
			        $final_output['message'] = "successfully";
			        $final_output['msg'] = "Profile Updated Successfully.";
			    	$final_output['data'] = $data_array;
		 		}
		 		else
			    {
	 				$final_output['status'] = 'false';
	 				$final_output['message'] = 'failed';
	 				$final_output['msg'] = 'Something went wrong! Please try again later.';
	 				unset($final_output['data']);
			    }		
	    }
	    else
	    {
	   	   $final_output['status'] = 'false';
	       $final_output['message'] = 'failed';
 	       $final_output['msg'] = 'Invalid Token';
	    }	
   }
   else
   {
	   	 $final_output['status'] = 'false';
		 $final_output['message'] = 'failed';
	 	 $final_output['msg'] = 'Unauthorised Access';
   }
   header("content-type: application/json");
   echo json_encode($final_output);
}

public function checktoken($token)
{
	$auth = $this->common_model->common_getRow('user',array('token'=>$token));

	if(!empty($auth))
	{
		$abc['status'] = "true";
		$abc['data'] =$auth;
		return $abc;
	}else
	{
		$abc['status'] = "false";
		return $abc;
	}
}

public function get_country()
{

   $country = $this->common_model->getData('countrycode',array(),'name','ASC');

   $final_output = array();
   if(!empty($country))
   {
      foreach ($country as $val) {

         $arr[] = array('name'=>$val->name,'alpha-2'=>$val->alpha,'country-code'=>$val->country_code,'phone-code'=>$val->phone_code);

      }

   	 
   	  $final_output['status'] = 'true';
   	  $final_output['message'] = 'successfully';
   	  $final_output['data'] = $arr;
       
   }
   else
   {
   	  $final_output['status'] = 'false';
   	  $final_output['message'] = 'failed';
   	  $final_output['msg']='Something went wrong! please try again later.';
   	  unset($final_output['data']);
   }	

    header("content-type: application/json");
    echo json_encode($final_output);


}  

public function get_location()
{
   $country =  $this->db->query("SELECT DISTINCT(country_id) FROM  location ")->result();

   $final_output = array();
   $country_name = '';
   $arr1 = array();
   if(!empty($country))
   {
      foreach ($country as $val) {

           $country_id = $val->country_id;
          	
	       $location = $this->common_model->common_getRow('countrycode',array('id'=>$country_id));
         //$location =  $this->db->query("SELECT * FROM  `countrycode` WHERE `id`= $country_id ORDER BY `name` ASC")->result();
           if($location)
	       {
	       		$country_name =  $location->name;
	       }
	       $arr11 = array();
	       $location = $this->common_model->getData('location',array('country_id'=> $country_id),'name','ASC');

	       foreach ($location as $key ) {
	       	
	       		$arr11[]  =  array('cityname'=>$key->name);

	       }
	       
            $arr1[] = array('country'=>$country_name,'citydata'=>$arr11);
      }
      	

   	  $final_output['status'] = 'true';
   	  $final_output['message'] = 'successfully';
   	  $final_output['data'] = $arr1;
       
   }
   else
   {
   	  $final_output['status'] = 'false';
   	  $final_output['message'] = 'failed';
   	  $final_output['msg']='City not found .';
   	  unset($final_output['data']);
   }	

    header("content-type: application/json");
    echo json_encode($final_output);


}

public function cuisine()
{
   $cuisine_data = $this->common_model->getData('cuisine',array(),'cuisine_name','ASC');
   $final_output = array();
   
   if(!empty($cuisine_data))
   {
   	foreach ($cuisine_data as $val) 
   	{
   		//$newelement = array('cuisine_name'=>'All Cuisine');
        $arr[] = array('cuisine_id'=>$val->cuisine_id,'cuisine_name'=>$val->cuisine_name);
	}
   	$aa = array('cuisine_id'=>0,'cuisine_name'=>'All Cuisines');
   	//array_push($arr, $aa);
   	array_unshift($arr, $aa);
   	//$arr['cuisine_name'] = 'All_Cuisine';
   		$final_output['status'] = 'true';
   	  	$final_output['message'] = 'successfully';
   	  	$final_output['data'] = $arr;
   }
   else
   {
   	  $final_output['status'] = 'false';
   	  $final_output['message'] = 'failed';
   	  $final_output['msg']='Something went wrong! please try again later.';
   	  unset($final_output['data']);
   }
   	header("content-type: application/json");
    echo json_encode($final_output);
}

public function restaurant_search()
{
   $headers = apache_request_headers();
   if($headers['secret_key'] !='')
   {
   	   	if($headers['secret_key'] == 'abcdABCD1234')
        {
          		$user_id = '123';
        }else
        {
          $check_key = $this->checktoken($headers['secret_key']);
          if($check_key['status'] == 'true')
          {
         	  	$user_id = $check_key['data']->user_id;
          }else
          {
            	$user_id = 'ABC';
          }
        }
   	    if(ctype_digit($user_id))
   	    {
		   $final_output = array();
		   $cuisine = $this->input->post('cuisine');
		   $type =$this->input->post('type');
		   $country = $this->input->post('country');
		   $city = $this->input->post('city');
		   $lat = $this->input->post('lat');
		   $lng = $this->input->post('lng');
		   if($user_id != '123')
		   {
		   		$where = "AND user_id='$user_id'";
		   }else
		   {
		   		$device_id = $this->input->post('device_id');
		   		$where = "AND device_id= '$device_id'";
		   }
		   $cu = '';
		   $data_array =array();
   		   	if($cuisine !='' && $cuisine != 'All Cuisines')
		    {
		      $cu = "AND FIND_IN_SET('$cuisine',cuisine)";
		    }
			if($type == 1)
			{
				$select = $this->db->query("SELECT restaurant_id,restaurant_name,service_type,status,address,contact_number,country_name,city_name,special,cuisine,payment_method,image FROM restaurant WHERE  country_name = '$country' AND city_name = '$city' ".$cu." order by restaurant_name ASC")->result();
			
			}else
			{
				//lat long query
				$select = $this->db->query("SELECT restaurant_id,restaurant_name,service_type,status,address,contact_number,country_name,city_name,special,cuisine,payment_method,image,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`map_lat`) ) 
                                      * cos( radians(`map_lng`) - radians('$lng')) + sin(radians('$lat')) 
                                      * sin( radians(`map_lat`))))) AS distance FROM restaurant HAVING distance <= '2' order by restaurant_name ASC")->result();
			}
			if($select==true)
            {
            	foreach ($select as $key) {
				
				$isfav = $this->db->query("SELECT user_id FROM favourite_restaurant WHERE restaurant_id = ".$key->restaurant_id." ".$where." ")->row();
				if($isfav)
				{
					$is_fav = 1;
				}else
				{
					$is_fav = 0;
				}
				$averate = $this->db->query("SELECT AVG(rating) as ave_rating FROM rating_review WHERE restaurant_id = ".$key->restaurant_id." ")->row()->ave_rating;
				if(!empty($averate)){ $ave_rat = round($averate,2); }else{ $ave_rat = 0; }

				$deals_arr = array();
				$deals = $this->common_model->getDataField('title,description,image','Hot_Deals',array('restaurant_id'=>$key->restaurant_id,'status'=>1));
				if($deals)
				{
					foreach ($deals as $value) {
						$deals_arr[] = $value;
					}
				}
				$image = '';
				if(!empty($key->image))
				{
					$image = base_url().'uploads/restaurant_image/'.$key->image;
				}
            	$data_array[] = array(
						'restaurant_id'=>$key->restaurant_id,
						'restaurant_name'=>$key->restaurant_name,
						'status'=>$key->status,
						'address'=>$key->address,
						'contact_number'=>$key->contact_number,
						'country'=>$key->country_name,
						'city'=>$key->city_name,	
						'avg_rating'=>$ave_rat,	
						'is_favourite'=>$is_fav,
						'image'=>$image,	
						'special'=>$key->special,
						'service_type'=>$key->service_type,
						'payment_method'=>$key->payment_method,
						'cuisine'=>$key->cuisine,
						'deals'=>$deals_arr
						);						         					
            	}
            }
        	if(!empty($data_array))
        	{
        	    $final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
   	  			$final_output['data'] = $data_array;
        	}else
        	{
        		$final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['msg'] = 'Restaurant not found';
   	  			$final_output['data'] = $data_array;
        	}	
		}
		else
		{
			$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
		}
   }
   else
   {
		$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
	}
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Add_rating_review()
{
   $headers = apache_request_headers();
   if($headers['secret_key'] !='')
   {
   	   $check_key = $this->checktoken($headers['secret_key']);
   	   if($check_key['status'] == 'true')
   	   {
  			$final_output = array();
		   	$restaurant_id = $this->input->post('restaurant_id');
		   	$rating =$this->input->post('rating');
		   	$review = $this->input->post('review');
   			$user_id = $check_key['data']->user_id;
		   	if(!empty($rating) && !empty($review) && $rating != 0)
		   	{
		   		$select = $this->common_model->getDataField("restaurant_id","restaurant",array('restaurant_id'=>$restaurant_id));
		   		if($select)
		   		{
		   			$selectrating = $this->common_model->getDataField("user_id","rating_review",array('restaurant_id'=>$restaurant_id,'user_id'=>$user_id));
		   			if($selectrating)
		   			{
		   				$ratingreview = $this->common_model->updateData("rating_review",array('rating'=>$rating,'review'=>$review,'update_at'=>militime),array('restaurant_id'=>$restaurant_id,'user_id'=>$user_id));
		   			}else
		   			{
		   				$ratingreview = $this->common_model->common_insert("rating_review",array('rating'=>$rating,'review'=>$review,'restaurant_id'=>$restaurant_id,'user_id'=>$user_id,'create_at'=>militime,'update_at'=>militime),array());
		   			}
		   			if($ratingreview)
		        	{
		        	    $final_output['status'] = 'true';
		   	  			$final_output['message'] = 'successfully';
			       		$final_output['msg'] = 'Rating and Review successfully added';
		   	  			//$final_output['data'] = $data_array;
		        	}else
		        	{
		        		$final_output['status'] = 'false';
		   	  			$final_output['message'] = 'failed';
			       		$final_output['msg'] = 'Something went wrong! please try again later.';
		        	}
		   		}else
		   		{
		   			$final_output['status'] = 'false';
	   	  			$final_output['message'] = 'failed';
		       		$final_output['msg'] = 'Restaurant not found';
		   		}
		   	}else
		   	{
				$final_output['status'] = 'false';
   	  			$final_output['message'] = 'failed';
	       		$final_output['msg'] = 'Request parameter not found';
		   	}
		}
		else
		{
			$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
		}
   }
   else
   {
		$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
	}
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Rating_review_list()
{
 
  			$final_output = array();
		   	$restaurant_id = $this->input->post('restaurant_id');
		   	$create_at = $this->input->post('create_at');
			if($create_at==0)
			{
				$create_at1= '';
			}else
			{
				$create_at1= "AND create_at < '$create_at'";
			}
		
		   	$arr = array();
	   		$select = $this->common_model->getDataField("restaurant_id","restaurant",array('restaurant_id'=>$restaurant_id));
	   		if($select)
	   		{
	   			
	   			$ratingreview = $this->db->query("SELECT * FROM `rating_review` WHERE `restaurant_id`='$restaurant_id' ".$create_at1." ORDER BY id DESC LIMIT 15")->result();

	   			if($ratingreview)
	        	{
	        		foreach ($ratingreview as $key) {

	        			$user_id = $key->user_id;

	        			$user = $this->common_model->common_getRow('user',array('user_id'=>$user_id));
	        			if(!empty($user)){ $username = $user->fullname;} else {  $username = ''; }

	        			$arr[] = array(
	        						'fullname'=>$username,
	        						'rating'=>$key->rating,
	        						'review'=>$key->review,
	        						'create_at'=>$key->create_at
	        				);
	        			}	
	        	}
	        	if(!empty($arr))
	        	{
	        	    $final_output['status'] = 'true';
	   	  			$final_output['message'] = 'successfully';
	   	  			$final_output['data'] = $arr;
	        	}
	        	else
	        	{
	        		$final_output['status'] = 'false';
	   	  			$final_output['message'] = 'failed';
		       		$final_output['msg'] = 'Rating and review not found';
	   	  			unset($final_output['data']);
	        	}
	   		}else
	   		{
	   			$final_output['status'] = 'false';
   	  			$final_output['message'] = 'failed';
	       		$final_output['msg'] = 'Restaurant not found';
	   		}

	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Add_to_favourite()
{
   $headers = apache_request_headers();
   if($headers['secret_key'] !='')
   {
   		if($headers['secret_key'] == 'abcdABCD1234')
        {
          		$user_id = '123';
        }else
        {
          $check_key = $this->checktoken($headers['secret_key']);
          if($check_key['status'] == 'true')
          {
         	  	$user_id = $check_key['data']->user_id;
          }else
          {
            	$user_id = 'ABC';
          }
        }
   	    if(ctype_digit($user_id))
   	    {
  			$final_output = array();
		   	$restaurant_id = $this->input->post('restaurant_id');
		   	$status = $this->input->post('status');
   			$device_id = $this->input->post('device_id');
		   	if(!empty($restaurant_id) && $restaurant_id != 0)
		   	{
		   		$select = $this->common_model->getDataField("restaurant_id","restaurant",array('restaurant_id'=>$restaurant_id));
		   		if($select)
		   		{
   					if($user_id == '123')
   					{
   						$user_id = 0;
   						$selectrating = $this->db->query("SELECT fav_id FROM favourite_restaurant WHERE restaurant_id='$restaurant_id' AND device_id ='$device_id'")->row();
   					}else
   					{
   						$selectrating = $this->db->query("SELECT fav_id FROM favourite_restaurant WHERE restaurant_id='$restaurant_id' AND user_id='$user_id'")->row();
   					}
   					if(!empty($selectrating))
		   			{
		   				if($status==0)
		   				{
		   					$favolist = $this->common_model->deleteData("favourite_restaurant",array('fav_id'=>$selectrating->fav_id));
		   					if($favolist)
		   					{
		   						$final_output['status'] = 'true';
				   	  			$final_output['message'] = 'successfully';
					       		$final_output['msg'] = 'Restaurant successfully removed from favourite list';
				   	 		}else
				   	 		{
				   	 			$final_output['status'] = 'false';
				   	  			$final_output['message'] = 'failed';
					       		$final_output['msg'] = 'Something went wrong! please try again later.';
				   	 		}
		   				}else{
		   					$final_output['status'] = 'true';
			   	  			$final_output['message'] = 'successfully';
				       		$final_output['msg'] = 'Restaurant already added to favourite list';
		   				}	
		   			}else
		   			{
		   				if($status==1)
		   				{
		   					$favolist = $this->common_model->common_insert("favourite_restaurant",array('restaurant_id'=>$restaurant_id,'user_id'=>$user_id,'device_id'=>$device_id,'create_at'=>militime));
		   					if($favolist)
		   					{
		   						$final_output['status'] = 'true';
				   	  			$final_output['message'] = 'successfully';
					       		$final_output['msg'] = 'Restaurant successfully added to favourite list';
				   	 		}else
				   	 		{
				   	 			$final_output['status'] = 'false';
				   	  			$final_output['message'] = 'failed';
					       		$final_output['msg'] = 'Something went wrong! please try again later.';
				   	 		}
		   				}else{
		   					$final_output['status'] = 'true';
			   	  			$final_output['message'] = 'successfully';
				       		$final_output['msg'] = 'Restaurant already removed from favourite list';
		   				}
		   			}
		   		}else
		   		{
		   			$final_output['status'] = 'false';
	   	  			$final_output['message'] = 'failed';
		       		$final_output['msg'] = 'Restaurant not found';
		   		}
		   	}else
		   	{
				$final_output['status'] = 'false';
   	  			$final_output['message'] = 'failed';
	       		$final_output['msg'] = 'Request parameter not found';
		   	}
		}
		else
		{
			$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
		}
   }
   else
   {
		$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
	}
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Favourite_list()
{
   $headers = apache_request_headers();
   if($headers['secret_key'] !='')
   {
   	   if($headers['secret_key'] == 'abcdABCD1234')
        {
          		$user_id = '123';
        }else
        {
          $check_key = $this->checktoken($headers['secret_key']);
          if($check_key['status'] == 'true')
          {
         	  	$user_id = $check_key['data']->user_id;
          }else
          {
            	$user_id = 'ABC';
          }
        }
   	   if(ctype_digit($user_id))
   	   {
  			$final_output = array();
		   	$arr = array();
		   	if($user_id != '123')
		   	{
		   		$where = array('favourite_restaurant.user_id'=>$user_id);
		   	}else
		   	{
		   		$device_id = $this->input->post('device_id');
		   		$where = array('favourite_restaurant.device_id'=>$device_id);
		   	}
   			$favlist = $this->common_model->getDataField("favourite_restaurant.fav_id,favourite_restaurant.restaurant_id,restaurant.restaurant_name,restaurant.special,restaurant.cuisine,restaurant.status,restaurant.country_name,restaurant.city_name,restaurant.image,restaurant.address,restaurant.contact_number","favourite_restaurant",$where,'favourite_restaurant.fav_id','DESC',array('restaurant'=>'favourite_restaurant.restaurant_id=restaurant.restaurant_id'));
   			if($favlist)
        	{
        		foreach ($favlist as $key) {

		        		$averate = $this->db->query("SELECT AVG(rating) as ave_rating FROM rating_review WHERE restaurant_id = ".$key->restaurant_id." ")->row()->ave_rating;
						if(!empty($averate)){ $ave_rat = round($averate,2); }else{ $ave_rat = 0; }
						
						$image = '';
						if(!empty($key->image))
						{
							$image = base_url().'uploads/restaurant_image/'.$key->image;
						}

					$arr[] = array(
						'restaurant_id'=>$key->restaurant_id,
						'restaurant_name'=>$key->restaurant_name,
						'status'=>$key->status,
						'address'=>$key->address,
						'contact_number'=>$key->contact_number,
						'country'=>$key->country_name,
						'city'=>$key->city_name,	
						'avg_rating'=>$ave_rat,	
						'is_favourite'=>1,
						'image'=>$image,	
						'special'=>$key->special,
						'cuisine'=>$key->cuisine

						);		
					}	
        	}
        	if(!empty($arr))
        	{
        	    $final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['data'] = $arr;
        	}
        	else
        	{
        		$final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['msg'] = 'Empty Favourite list';
   	  			$final_output['data'] = $arr;
        	}
	 	}
		else
		{
			$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
		}
   }
   else
   {
		$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
	}
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Restaurant_info()
{
   
  			$restaurant_id = $this->input->post('restaurant_id');
  			$final_output = array();
		   	$arr = (object)array();
	   		
			$restaurantd = $this->db->query("SELECT restaurant_id,cuisine,service_type,about,Minimum_order_value,delivery_charges,open_time,delivery_time,payment_method,closed_time,special,address,map_lat,map_lng,city_name,country_name FROM restaurant WHERE  restaurant_id = '$restaurant_id'")->row();
   			if($restaurantd)
        	{
        		$newDateTimeopen = date('h:i A', strtotime($restaurantd->open_time));
        		$newDateTimeclose = date('h:i A', strtotime($restaurantd->closed_time));
        		//$fulltime = $newDateTimeopen. '-' .$newDateTimeclose;
        		$galary = array();	$image = array();  $video = '';
        		$galry = $this->common_model->getData("gallery",array('restaurant_id'=>$restaurant_id));
        		if($galry)
        		{
        			foreach ($galry as $key) {
        				if($key->file_type == 1){ $image[] = array('image'=>base_url().'uploads/gallery/'.$key->image); }
        				else { $video = $key->image; }
        			}
        			$galary[] = array(
        						'image'=>$image,
        						'video'=>$video
        						);
        		}


        		$arr = array(
						'restaurant_id'=>$restaurantd->restaurant_id,
						'about'=>$restaurantd->about,
						'Minimum_order_value'=>$restaurantd->Minimum_order_value,
						'delivery_charges'=>$restaurantd->delivery_charges,
						'delivery_time'=>$restaurantd->delivery_time,
						'payment_method'=>$restaurantd->payment_method,
						'cuisine'=>$restaurantd->cuisine,
						'service_type'=>$restaurantd->service_type,
						'map_lat'=>$restaurantd->map_lat,
						'map_lng'=>$restaurantd->map_lng,	
						'open_time'=>$newDateTimeopen,	
						'close_time'=>$newDateTimeclose,
						'special'=>$restaurantd->special,
						'address'=>$restaurantd->address.', '.$restaurantd->city_name.', '.$restaurantd->country_name,
						'gallary'=>$galary
					    );	
        	}
        	if($arr != (object)array())
        	{
        	    $final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
   	  			$final_output['data'] = $arr;
        	}
        	else
        	{
        		$final_output['status'] = 'false';
   	  			$final_output['message'] = 'failed';
	       		$final_output['msg'] = 'Restaurant info not found';
   	  			unset($final_output['data']);
        	}
  
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Add_to_Cart()
{
   $headers = apache_request_headers();
   if($headers['secret_key'] !='')
   {
   	   $check_key = $this->checktoken($headers['secret_key']);
   	   if($check_key['status'] == 'true')
   	   {
			$userid = $check_key['data']->user_id;
  			$restaurant_id = $this->input->post('restaurant_id');
  			$item_id = $this->input->post('item_id');
  			$quantity = $this->input->post('quantity');
  			$final_output = array();
		   	$arr = (object)array();
	   		
			if($quantity > 0)
	        {
	          	$sel_cart = $this->common_model->common_getRow("eatapp_user_cart",array('user_id'=>$userid,'restaurant_id'=>$restaurant_id,'item_id'=>$item_id));
	            if($sel_cart)
	          	{
		            $updatecart = $this->common_model->updateData("eatapp_user_cart",array('quantity'=>$quantity,'update_at'=>militime),array('user_id'=>$userid,'restaurant_id'=>$restaurant_id,'item_id'=>$item_id));
		            if($updatecart)
		            {
		              	$final_output['status'] ="true";
		              	$final_output['message'] ="successfully";
		              	$final_output['msg'] ="Cart successfully updated";
		              	$final_output['data'] = array('cart_id'=>$sel_cart->cart_id);  
		            }else
		            {
		              	$final_output['status'] ="false";
		              	$final_output['message'] ="failed";
		              	$final_output['msg'] ="Something went wrong! please try again later.";
		              	unset($final_output['data']);
		            }
	          	}else
	          	{
		            $insercart = $this->common_model->common_insert("eatapp_user_cart",array('user_id'=>$userid,'restaurant_id'=>$restaurant_id,'item_id'=>$item_id,'quantity'=>$quantity,'create_at'=>militime,'update_at'=>militime),array());
		            if($insercart)
		            {
		              	$final_output['status'] ="true";
		              	$final_output['message'] ="successfully";
		              	$final_output['msg'] ="Item added to cart";
		              	$final_output['data'] =  array('cart_id'=>$insercart);
		            }else
		            {
		             	$final_output['status'] ="false";
		              	$final_output['message'] ="failed";
		              	$final_output['msg'] ="Something went wrong! please try again later.";
		              	//$final_output['data']= $arr;
		            }
	          	}
	        }else
	        {
	          	$final_output['status'] ="true";
	          	$final_output['message'] ="failed";
	          	$final_output['msg'] ="You have to select atleast one quantity";
	          	unset($final_output['data']);
	        }
	 	}
		else
		{
			$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
		}
   }
   else
   {
		$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
	}
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Item_remove_from_cart()
{
   $headers = apache_request_headers();
   if($headers['secret_key'] !='')
   {
   	   $check_key = $this->checktoken($headers['secret_key']);
   	   if($check_key['status'] == 'true')
   	   {
			$userid = $check_key['data']->user_id;
  			$cart_id = $this->input->post('cart_id');
  			$final_output = array();
			
          	$sel_cart = $this->common_model->common_getRow("eatapp_user_cart",array('cart_id'=>$cart_id));
            if($sel_cart)
          	{
	            $updatecart = $this->common_model->deleteData("eatapp_user_cart",array('cart_id'=>$cart_id));
	            if($updatecart)
	            {
	              	$final_output['status'] ="true";
	              	$final_output['message'] ="successfully";
	              	$final_output['msg'] ="Item removed from cart";
	            }else
	            {
	              	$final_output['status'] ="false";
	              	$final_output['message'] ="failed";
	              	$final_output['msg'] ="Something went wrong! please try again later.";
	            }
          	}else
          	{
	            $final_output['status'] ="false";
              	$final_output['message'] ="failed";
              	$final_output['msg'] ="Item already removed from cart";
          	}
	 	}
		else
		{
			$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
		}
   }
   else
   {
		$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
	}
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Get_deals()
{
	$headers = apache_request_headers();
    if($headers['secret_key'] !='')
    {
   	   if($headers['secret_key'] == 'abcdABCD1234')
        {
          		$user_id = '123';
        }else
        {
          $check_key = $this->checktoken($headers['secret_key']);
          if($check_key['status'] == 'true')
          {
         	  	$user_id = $check_key['data']->user_id;
          }else
          {
            	$user_id = 'ABC';
          }
        }
   	    if(ctype_digit($user_id))
   	    {
			$final_output = array();
			$data_array = array();
			/*$create_at = $this->input->post('create_at');
			if($create_at==0)
			{
				$create_at1= '';
			}else
			{
				$create_at1= "AND create_at < '$create_at'";
			}*/

			if($user_id != '123')
		   	{
		   		$where = "AND user_id='$user_id'";
		   	}else
		   	{
		   		$device_id = $this->input->post('device_id');
		   		$where = "AND device_id= '$device_id'";
		   	}

			$date = date('Y-m-d 00:00:00');
		    $offer_data = $this->db->query("SELECT * FROM `Hot_Deals` WHERE `start_date` <= '$date' AND end_date >= '$date' AND `status`=1 ORDER BY deals_id DESC")->result();
		    if($offer_data==TRUE)
		    {
			    foreach($offer_data as $key)
			    {
					$select = $this->db->query("SELECT restaurant_id,restaurant_name,service_type,status,address,contact_number,country_name,city_name,special,cuisine,payment_method,image FROM restaurant WHERE restaurant_id = ".$key->restaurant_id."")->row();
		        	
		        	$isfav = $this->db->query("SELECT user_id FROM favourite_restaurant WHERE restaurant_id = ".$key->restaurant_id." ".$where." ")->row();
					if($isfav)
					{
						$is_fav = 1;
					}else
					{
						$is_fav = 0;
					}
					$averate = $this->db->query("SELECT AVG(rating) as ave_rating FROM rating_review WHERE restaurant_id = ".$key->restaurant_id." ")->row()->ave_rating;
					if(!empty($averate)){ $ave_rat = round($averate,2); }else{ $ave_rat = 0; }

					$image = '';
					if(!empty($select->image))
					{
						$image = base_url().'uploads/restaurant_image/'.$select->image;
					}
	          					         					

					$offer_image = '';
					if(!empty($key->image))
					{
						$offer_image = base_url().'uploads/offer_image/'.$key->image;
					}

			     	$data_array[] = array(
							'title'=>$key->title,
							'description'=>strip_tags($key->description),
							'deal_image'=>$offer_image,
							'end_date'=>date('Y/m/d',strtotime($key->end_date)),
							'restaurant_id'=>$key->restaurant_id,
							'restaurant_name'=>$select->restaurant_name,
							'status'=>$select->status,
							'address'=>$select->address,
							'contact_number'=>$select->contact_number,
							'country'=>$select->country_name,
							'city'=>$select->city_name,	
							'avg_rating'=>$ave_rat,	
							'is_favourite'=>$is_fav,
							'image'=>$image,	
							'special'=>$select->special,
							);	
			    }	
		    }
    		if(!empty($data_array))
        	{
        	    $final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['data'] = $data_array;
        	}
        	else
        	{
        		$final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['msg'] = 'Offers Not Found';
   	  			$final_output['data'] = $data_array;
        	}
        }else
        {
        	$final_output['status'] = 'false';
       		$final_output['message'] = 'failed';
	       	$final_output['msg'] = 'Invalid Token';
        }	
    }else
    {
    	$final_output['status'] = 'false';
		$final_output['message'] = 'failed';
	 	$final_output['msg'] = 'Unauthorised Access';
    }
	header("content-type: application/json");
    echo json_encode($final_output);
}

public function Restaurant_Menu()
{
   
  		$restaurant_id = $this->input->post('restaurant_id');
  			$final_output = array();
		   	$finalarr = array();
			$menulist = $this->common_model->getData("menu",array('restaurant_id'=>$restaurant_id),'menu','ASC');
   			if($menulist)
        	{
        		foreach ($menulist as $key) {

					$submenu = $this->common_model->getDataField("submenu,submenu_title,description,variant_title,variant_option_price","submenu",array('menu_id'=>$key->menu_id),'id','DESC');			
					$arr=array();
					if(!empty($submenu))
					{
						foreach ($submenu as $value) {
							
							$variant = array();
						//	if(!empty($value->variant_option_price)){
							$vararr = json_decode($value->variant_option_price);
							for ($i=0; $i < count($vararr->attribute); $i++) { 
								$variant[] = array(
											'attribute'=>	$vararr->attribute[$i],
											'price'=>	$vararr->price[$i],
											);
								}
						//	}
							$arr[] = array(
								'submenu'=>$value->submenu,
								'submenu_title'=>$value->submenu_title,
								'description'=>$value->description,
								'variant_title'=>$value->variant_title,
								'variant_option_price'=>$variant
								);		
						}	
					$finalarr[] = array('menu'=>$key->menu,'submenu'=>$arr);
					}
				}

        	} 
         	if(!empty($finalarr))
        	{
        	    $final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['data'] = $finalarr;
        	}
        	else
        	{
        		$final_output['status'] = 'true';
   	  			$final_output['message'] = 'successfully';
	       		$final_output['msg'] = 'No menu list found';
   	  			$final_output['data'] = $finalarr;
        	}

	header("content-type: application/json");
    echo json_encode($final_output);
}



}