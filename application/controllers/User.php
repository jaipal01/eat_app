<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id'))
		{
			redirect(base_url('login'));
		}
		
	}

	public function index()
	{
		$data['user_data'] = $this->common_model->getData('user',array(),'user_id','DESC');
		$this->load->view('admin/user/show_user',$data);
	}
	
	public function show_user()
	{
		$data['user_data'] = $this->common_model->getData('user',array(),'user_id','DESC');
		$this->load->view('admin/user/show_user',$data);

	}
	
	public function change_status()
	{
	$user_id = $this->input->post('user_id');
	$admin_status = $this->input->post('admin_status');
    
    $update = $this->common_model->updateData("user",array('admin_status'=>$admin_status),array('user_id'=>$user_id));
       if($update)
       {
    	  redirect('user/show_user');
       }
    }
    public function add_city()
    
    { 
        $data['country_data'] = $this->common_model->getData("countrycode",array(),'id','ASCE');
    	if(isset($_POST['submit'])){
    	$data1 = array(
					'country_id' =>$this->input->post('country'),
					'name' =>$this->input->post('city')
					   );
			$location = $this->common_model->common_insert('location',$data1);
            if($location){
            	            $this->session->set_flashdata('success', 'Location Inserted Successfully.');
					  		redirect('user/add_city');
            }                      
    	}
    	$this->load->view('admin/user/add_city',$data);
    }
    
    public function update_version()
	{
		$data['version_data'] = $this->common_model->getData('App_version',array(),'version_id','DESC');
		$this->load->view('admin/user/update_version',$data);

	}
    
    public function add_version()
	{
	
    $data['version_data'] = $this->common_model->common_getRow('App_version');
	
       if(isset($_POST['submit'])){
    	$version_id = $this->input->post('id');
    	$data1 = array(				
						'version_name' =>$this->input->post('v_name'),
						'version_code' =>$this->input->post('v_code'),
					    'update_requirment' =>$this->input->post('u_requirment')
						 );
    	
	       $update = $this->common_model->updateData("App_version",$data1,array('version_id'=>$version_id));
           if($update)
           {
        	  $this->session->set_flashdata('success', ' Version successfully updated.');
    	      redirect('user/add_version');
           }
       }

    $this->load->view('admin/user/add_version',$data); 
    } 
     
    public function show_country()
    {

           $data['country_data'] = $this->common_model->getData('countrycode',array(),'id','DESC');
	       $this->load->view('admin/user/show_country',$data);
    }
    
    public function add_country()
    {
    	if(isset($_POST['submit']))
    	{
    	$data1 = array(				
					'name' =>$this->input->post('country_name'),
					'alpha' =>$this->input->post('alphabet'),
					'country_code' =>$this->input->post('c_code'),
					'phone_code' =>$this->input->post('p_code'),
					  );

           $insert =$this->common_model->common_insert('countrycode',$data1); 
           if ($insert) 
           {
        	$this->session->set_flashdata('success', ' Country successfully inserted.');
    	    redirect('user/add_country');
           }
          
        }
    $this->load->view('admin/user/add_country');
    }

    public function show_city($country_id = false)
	{
		$data['city_data'] = $this->common_model->getData('countrycode');

		if($country_id)
		{
			$where = array('country_id'=>$country_id);
		}	
		else
		{
			$where = array('country_id'=>1);
		}

	    $data['show_data'] = $this->common_model->getData('location',$where,'id','DESC');
                      
        $this->load->view('admin/user/show1_city',$data);
        
    }
}
