<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant extends CI_Controller {

public function __construct()
{
	parent::__construct();
	$militime=round(microtime(true) * 1000);
	define('militime', $militime);
	if(!$userid = $this->session->userdata('admin_id'))
	{
		redirect(base_url('login'));
	}
	
}

public function index()
{
	$data['cuisine'] = $this->common_model->getData('cuisine');	

	$data['country'] = $this->common_model->getData('countrycode');	

	$this->load->view('admin/subadmin/add_rest',$data);
}
	
public function add_restaurant()
{
  $restaurant_owner = $this->session->userdata('admin_id');
   if(isset($_POST['submit']))
   {
       $rest_image ='';
      
       if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
       {
         
            $_FILES['file_url']['name']= $_FILES['image']['name'];
            $_FILES['file_url']['type']= $_FILES['image']['type'];
            $_FILES['file_url']['tmp_name']= $_FILES['image']['tmp_name'];
            $_FILES['file_url']['error']= $_FILES['image']['error'];
            $_FILES['file_url']['size']= $_FILES['image']['size'];  

            $config = array();
            $config['upload_path']   = 'uploads/restaurant_image/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';

            $subFileName = explode('.',$_FILES['file_url']['name']);
            $ExtFileName = end($subFileName);
            $config['file_name'] = md5(militime.$_FILES['file_url']['name']).'.'.$ExtFileName;

            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('file_url'))
            { 
              $image_data = $this->upload->data();
              $rest_image =  $image_data['file_name'];
            }else
            {  
              $err= $this->upload->display_errors();
              $final_output = $this->session->set_flashdata('error_pic', $err);
            }
        }

		 $data['country_data'] = $this->common_model->common_getRow('countrycode',array('id'=>$this->input->post('country')));

    		if(!empty($data['country_data']))
    		{
    			$country_name = $data['country_data'] ->name;
    		}
    		else
    		{
    			$country_name = '';
    		}

    		if(!empty($this->input->post('city')))
    		{
    		  $data['city_data'] = $this->common_model->common_getRow('location',array('id'=>$this->input->post('city')));

    		  if(!empty($data['city_data']))
    		  {
                 $city_name = $data['city_data']->name;
    		  }	
    		}
    		else
    		{
    			$city_name  = '';
    		}	

			$address = $this->input->post('restaurant_name').' '.$this->input->post('address').' '.$city_name.' '.$country_name;

			   	$string = str_replace(" ","+",$address);
				$chk_url='http://maps.google.com/maps/api/geocode/json?address='.$string.'&sensor=false';
				$geocode = file_get_contents($chk_url);
				$output = json_decode($geocode);
				
				$lat = $output->results[0]->geometry->location->lat;
				$lng = $output->results[0]->geometry->location->lng;

				$multi_cuisine = implode(',', $this->input->post('cuisine_name'));

				$multi_method = implode(',', $this->input->post('method'));

			    $data1 = array(	  			
						'restaurant_name' =>$this->input->post('restaurant_name'),
            'restaurant_owner_id'=>$restaurant_owner,
						'cuisine'=>$multi_cuisine,
						'about' =>$this->input->post('about_restaurant'),
						'open_time' =>$this->input->post('open_time'),
						'closed_time' =>$this->input->post('closed_time'),
						'address' =>$this->input->post('address'),
						'status' =>2,
						'contact_number' =>$this->input->post('contact_number'),
						'service_type' =>$this->input->post('delivery_service'),
						'delivery_Charges' =>$this->input->post('delivery_Charges'),
						'country_id' =>$this->input->post('country'),
						'city_id' =>$this->input->post('city'),
						'country_name'=>$country_name,
						'city_name'=>$city_name,
						'map_lat'=>$lat,
						'map_lng'=>$lng,
						'payment_method'=>$multi_method,
						'delivery_time' =>$this->input->post('delivery_Time'),
						'special' =>$this->input->post('special'),
						'Minimum_order_value' =>$this->input->post('minimum_order_value'),
						'image'=> $rest_image,
						'create_at'=>militime,
						'update_at'=>militime
						);

       		$insert =$this->common_model->common_insert('restaurant',$data1); 

            if ($insert) 
            {
	        	$this->session->set_flashdata('success', ' Restaurant successfully added.');
	    	    redirect('restaurant');
            }
      
   }
      $this->load->view('admin/subadmin/add_rest');
}
    
public function show_restaurant()
{
	$subadminid = $this->session->userdata('admin_id');
	$data['show_restaurant'] = $this->common_model->getData('restaurant',array('restaurant_owner_id'=>$subadminid),'restaurant_id','DESC');

	$this->load->view('admin/subadmin/show_rest',$data);
}

public function get_city()
{
  $country_id = $this->input->post('country_id');

	$country_data = $this->common_model->getData('location',array('country_id'=>$country_id),'name','ASC');
	?>
	<option value="">Select City</option>
	<?php
	foreach ($country_data as $key) 
	{ 
	?>
 		<option value="<?php echo $key->id; ?>"><?php echo $key->name;?></option>
   <?php
}

}

public function gallery()
{
   $subadmin_id = $this->session->userdata('admin_id');	
   if(isset($_POST['submit']))
   {	
        if(isset($_FILES['image']['name'][0]) && $_FILES['image']['name'][0] != '')
	    {  
            $files = $_FILES;	
        	$filesCount1 = count($_FILES['image']['name']);
	        	    
	        for($i = 0; $i < $filesCount1; $i++)
	        {
    	            $_FILES['image']['name'] =  $files['image']['name'][$i];
    	            $_FILES['image']['type'] =   $files['image']['type'][$i];
	                $_FILES['image']['tmp_name'] =  $files['image']['tmp_name'][$i];
	                $_FILES['image']['error'] =  $files['image']['error'][$i];
	                $_FILES['image']['size'] =  $files['image']['size'][$i];

	                 $date = date("ymdhis"); 	
    	             $uploadPath = 'uploads/gallery/';
                     $config['upload_path'] = $uploadPath;
                     $config['allowed_types'] = '*';


                        $subFileName = explode('.',$_FILES['image']['name']);
                        $ExtFileName = end($subFileName);

              	    	$config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;

                    	$fileName = $config['file_name'];
                   		$fileName11[] = $config['file_name'];
                   		
                   		$this->load->library('upload', $config);
                   		$this->upload->initialize($config);

               		   
               			if($this->upload->do_upload('image'))
                   		{
                    	  $fileData = $this->upload->data();
                          $uploadData[$i]['file_name'] = $fileData['file_name'];
                    	}

                    	$restaurant_image = array('restaurant_id'=>$subadmin_id,'image'=>$uploadData[$i]['file_name'],'file_type'=>1,'create_at'=>militime);

                	   $insert_image = $this->common_model->common_insert('gallery',$restaurant_image);
	        } 

	        	         
	    }

        $video_url = $this->input->post('video_url');

        if(!empty($video_url))
        {
        	$insert =$this->common_model->common_insert('gallery',array('restaurant_id'=>$subadmin_id,'image'=>$video_url,'file_type'=>2,'create_at'=>militime)); 
        }	
      	

      	$this->session->set_flashdata('success', 'Files successfully added.');
	    redirect('restaurant/show_restaurant');
    }

   $this->load->view('admin/subadmin/gallery');
}  

public function gallery_detail($restaurant_owner_id = false)
{
  $data['restaurant_images'] = $this->common_model->getData('gallery',array('restaurant_id'=>$restaurant_owner_id));
  $this->load->view('admin/subadmin/gallery_detail',$data);
}

public function add_offers()
{


  $userid = $this->session->userdata('admin_id');
  if(isset($_POST['submit']))
  {        
    $offer_image ='';
     
    if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
		{ 
				$date = date("ymdhis");
				$config['upload_path'] = 'uploads/offer_image/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				
				$subFileName = explode('.',$_FILES['image']['name']);
				$ExtFileName = end($subFileName);
				$config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;
                
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('image'))
				{ 
           $upload_data = $this->upload->data();
					 $offer_image = $upload_data['file_name'];
				}
				else
				{		
					 $this->data['err']= $this->upload->display_errors();
					 $this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg File Type.');
				 	 redirect('restaurant/add_offers');
				}
		}
       $submenu_id = $this->input->post('submenu');
       $data1 = array(    
                    'title' =>$this->input->post('title_name'),
                    'menu_id'=>$this->input->post('menu'),
                    'submenu_id'=>$submenu_id,
                    'discount_type'=>$this->input->post('discount_type'),
                    'discount'=>$this->input->post('discount'),
                    'description' =>$this->input->post('description'),
                    'start_date' =>$this->input->post('start_date'),
                    'end_date' =>$this->input->post('end_date'),
                    'image' => $offer_image,
                    'restaurant_id' => $userid,
                    'create_at'=>militime,
                    'update_at'=>militime
                    );

      // print_r($data1);exit;
           $update =  $this->common_model->updateData("submenu",array('is_deal' =>1),array('id'=>$submenu_id));
           $insert =$this->common_model->common_insert('Hot_Deals',$data1);
           if($insert)
           {
                $this->session->set_flashdata('success', 'Offer successfully added.');
                redirect('restaurant/show_offers');
           }
   }

   $data['restaurant_name']  = $this->common_model->getData('restaurant',array('restaurant_owner_id'=>$userid));


   $this->load->view('admin/subadmin/add_offers',$data);    
}

public function show_offers()
{  
   $userid = $this->session->userdata('admin_id');
  $data['user_data'] = $this->common_model->getData('Hot_Deals',array('restaurant_id'=>$userid),'deals_id','DESC');
   $this->load->view('admin/subadmin/show_offers',$data);
}

public function change_status()
{
    $deals_id = $this->input->post('deals_id');
    $status = $this->input->post('status');
   
    $update = $this->common_model->updateData("Hot_Deals",array('status'=>$status),array('deals_id'=>$deals_id));
      if($update)
      {
         echo '1000';exit; 
      }
}

public function offer_delete($deal_id = false)
{
    $delete = $this->common_model->deleteData("Hot_Deals",array('deals_id'=>$deal_id));
    if($delete)
    {
         echo '1000';exit; 
    }
}
public function edit_offer($deal_id = false)
{
      $data['offer_data'] = $this->common_model->common_getRow('Hot_Deals',array('deals_id'=>$deal_id));	
      
      if(isset($_POST['submit']))
      {        
           if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
		   { 
					$date = date("ymdhis");
					$config['upload_path'] = 'uploads/offer_image/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					$subFileName = explode('.',$_FILES['image']['name']);
					$ExtFileName = end($subFileName);
					$config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;
                    
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('image'))
					{ 
                         $upload_data = $this->upload->data();
						 $offer_image = $upload_data['file_name'];
					}
					else
					{		
						 $this->data['err']= $this->upload->display_errors();
						 $this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg File Type.');
					 	 redirect('restaurant/edit_offers/'.$deal_id);
					}
			}
            else
            {
            	 $offer_image = $data['offer_data']->image;
            }	

           $data1 = array(    
                        'title' =>$this->input->post('title_name'),
                        'description' =>$this->input->post('description'),
                        'start_date' =>$this->input->post('start_date'),
                        'end_date' =>$this->input->post('end_date'),
                        'image' => $offer_image
                      );
            $update =  $this->common_model->updateData("Hot_Deals",$data1,array('deals_id'=>$deal_id));
            if($update)
            {
                $this->session->set_flashdata('success', 'Offer successfully Edited.');
                redirect('restaurant/show_offers');
            }
       }
              
       $this->load->view('admin/subadmin/edit_offer',$data);    
}

public function menu()
{
    $userid = $this->session->userdata('admin_id');
     if(isset($_POST['submit']))
     {    
	      $menu = $this->input->post('menu');
        $restaurant_id = $this->input->post('restaurant_id');
	      $insert = $this->common_model->common_insert('menu',array('menu'=>$menu,'restaurant_id'=>$restaurant_id,'restaurant_owner_id'=>$userid));
	      if($insert)
	      {
	            $this->session->set_flashdata('success', 'Menu successfully added.');
	            redirect('restaurant/mainmenu_list');
	      }      
     }
      $data['restaurant'] = $this->common_model->getData('restaurant',array('restaurant_owner_id'=>$userid),'restaurant_name','ASC');

       $this->load->view('admin/subadmin/menu',$data);
}

public function submenu()
{
    $userid = $this->session->userdata('admin_id');

    $data['restaurant'] = $this->common_model->getData('restaurant',array('restaurant_owner_id'=>$userid),'restaurant_name','ASC');
   
     //$data['menu'] = $this->common_model->getData('menu',array('restaurant_id'=>$userid),'menu_id','ASC');

      if(isset($_POST['submit']))
      { 
            $menu_id = $this->input->post('menu_id');
            $submenu = $this->input->post('submenu');
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $variant_title = $this->input->post('variant_title');
            $option_arr = $this->input->post('option');
            $price_arr = $this->input->post('price');
            $restaurant_id = $this->input->post('restaurant_id');

            $related_product_arr = $this->input->post('product');

            $related_product = implode(',',$related_product_arr);
            if ($related_product == '') {
              $related_product ='';
            }
            $attr['attribute'] = $option_arr;
            $price['price'] = $price_arr;
            $arr = json_encode(array_merge($attr, $price));
            $submenu_array = array('menu_id'=>$menu_id,
                                  'submenu'=>$submenu,
                                  'restaurant_owner_id'=>$userid,
                                  'restaurant_id'=> $restaurant_id,
                                  'submenu_title'=>$title,
                                  'description'=>$description,
                                  'variant_title'=>$variant_title,
                                  'variant_option_price'=>$arr,
                                  'related_product'=>$related_product,
                                  'create_at'=>militime
                                  );

            $insert_id = $this->common_model->common_insert('submenu',$submenu_array);

            if($insert_id > 0)
            {
                if(isset($_FILES['image']['name'][0]) && $_FILES['image']['name'][0] != '')
                {  
                    $files = $_FILES; 
                    $filesCount1 = count($_FILES['image']['name']);
                  
                      for($i = 0; $i < $filesCount1; $i++)
                      {

                         if($i == 0)
                         {
                            $j = 1;
                         } 
                         else
                         {
                            $j = $i+1;
                         } 

                          $_FILES['image']['name'] =  $files['image']['name'][$i];
                          $_FILES['image']['type'] =   $files['image']['type'][$i];
                          $_FILES['image']['tmp_name'] =  $files['image']['tmp_name'][$i];
                          $_FILES['image']['error'] =  $files['image']['error'][$i];
                          $_FILES['image']['size'] =  $files['image']['size'][$i];

                           
                             $uploadPath = 'uploads/submenu_image/';
                             $config['upload_path'] = $uploadPath;
                             $config['allowed_types'] = '*';

                                $subFileName = explode('.',$_FILES['image']['name']);
                                $ExtFileName = end($subFileName);

                                $_FILES['image']['name'] = 'submenuimage';

                                $config['file_name'] = $_FILES['image']['name'].'-'.$insert_id.'_'.$j.'.'.$ExtFileName;

                                $fileName = $config['file_name'];
                                $fileName11[] = $config['file_name'];

                                $this->load->library('upload', $config);
                                $this->upload->initialize($config);
                              
                                if($this->upload->do_upload('image'))
                                {
                                    $fileData = $this->upload->data();
                                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                                }
                      } 
                        
                }
            }  

         $this->session->set_flashdata('success', 'Submenu successfully added.');
         redirect('restaurant/menu_list');
      }

    $data['submenu']  = $this->common_model->getData('submenu',array('restaurant_owner_id'=>$userid));
  
    $this->load->view('admin/subadmin/submenu',$data);
}

public function menu_list($restaurant_id = false)
{
    $userid = $this->session->userdata('admin_id');
    $data['restaurant_data'] = $this->common_model->getData('restaurant',array('restaurant_owner_id'=>$userid),'restaurant_id','ASC');
    if($restaurant_id)
    {
      $where = array('restaurant_id'=>$restaurant_id);
    } 
    $data['submenu'] = $this->common_model->getData('submenu',$where,'menu_id','ASC');
   
    $this->load->view('admin/subadmin/menu_list',$data);
}

public function mainmenu_list($restaurant_id = false)
{
    $userid = $this->session->userdata('admin_id');
    $data['restaurant_data'] = $this->common_model->getData('restaurant',array('restaurant_owner_id'=>$userid),'restaurant_id','ASC');
    if($restaurant_id)
    {
      $where = array('restaurant_id'=>$restaurant_id);
    }   
    $data['menu'] = $this->common_model->getData('menu',$where,'menu_id','ASC');
    $this->load->view('admin/subadmin/main_menulist',$data);
}
public function get_submenu()
{
  $menu_id = $this->input->post('menu_id');

  $submmenu_data = $this->common_model->getData('submenu',array('menu_id'=>$menu_id),'submenu','ASC');
  ?>
  
  <?php
  foreach ($submmenu_data as $key) 
  { 
  ?>
    <option value="<?php echo $key->id; ?>"><?php echo $key->submenu;?></option>
   <?php
  }
}

public function get_menu()
{
  $restuarant_id = $this->input->post('restaurant_id');

  $menu_data = $this->common_model->getData('menu',array('restaurant_id'=>$restuarant_id),'menu','ASC');
  ?>
   <option value="">Select Menu</option>
  <?php
  foreach ($menu_data as $key) 
  { 
  ?>
    <option value="<?php echo $key->menu_id; ?>"><?php echo $key->menu;?></option>
   <?php
  }
}

    
}	
