<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		date_default_timezone_set('Asia/Kolkata');
		date_default_timezone_set('Asia/Kolkata');

		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}
	
	public function index()
	{ 
	   $data['admin_data'] = $this->common_model->common_getRow('admin',array('admin_id'=>'1'));
	   $this->load->view('admin/profile',$data);

	}
	
  
public function user_detail()
{
   $data['user_data'] = $this->common_model->getData('signup',array('del_status'=>0),'user_id','DESC');
   //print_r($data['user_data']);exit;
   $this->load->view('admin/user/user_detail',$data);

}
public function check_password()
{
	$password = $this->input->post('password'); 
	$query=$this->db->query("SELECT `password` FROM `Admin_table` WHERE `password` = '".$password."'");
	if ($query->num_rows() > 0){
		echo "10000";
	}
}

public function edit()
{ //die('nmbhsd');
   $admin_id = $this->session->userdata('admin_id');
   $data['admin_data'] = $this->common_model->common_getRow('admin',array('admin_id'=>$admin_id));

   if(isset($_POST['submit']))
   { 
      if(isset($_FILES['admin_img']['name']) && $_FILES['admin_img']['name'] != '')
	  {  //die('hiigg');
			$date = date("ymdhis");
			$config['upload_path'] = 'uploads/admin_image/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			
			$subFileName = explode('.',$_FILES['admin_img']['name']);
			$ExtFileName = end($subFileName);
			$config['file_name'] = md5($date.$_FILES['admin_img']['name']).'.'.$ExtFileName;
            
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('admin_img'))
			{ 
                 $upload_data = $this->upload->data();
				
				 $admin_image =  $upload_data['file_name'];
				 //$product_image1 = $config['upload_path'].$upload_data['file_name'];
			}else
			{  
				$this->data['err']= $this->upload->display_errors();
				$this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg,gif File Type.');		
				redirect('profile');
			}
		
	 }
	 else
	 { 
	   $admin_image = $data['admin_data']->image;
	 }


   	   $admin_data = array(
					'name'=>$this->input->post('name'),
					'image'=>$admin_image
					);
   	   //print_r($admin_data); exit;
   	 }
   	 
   	 if(isset($_POST['submit1']))
   	 {
   	 	$old_password = md5($this->input->post('O_password')); 
        $data['admin_data'] = $this->common_model->common_getRow('admin',array('admin_id'=>$admin_id));
        if($data['admin_data']->password!=$old_password)
		{ 
			$this->session->set_flashdata('fail', 'Invalid old password');
			redirect('profile/');
		}
		
   	 	$admin_data = array(
					'password'=>md5($this->input->post('c_password'))
					);

   	 }	
	     $update = $this->common_model->updateData('admin',$admin_data,array('admin_id'=>$admin_id));
	  

   	  $this->session->set_flashdata('success', 'updated Successfully.');
      redirect('profile');
  	  //$this->load->view('admin/tender/edit_tender',$data);
}

}
